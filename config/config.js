require('dotenv').config();//instatiate environment variables

let CONFIG = {} //Make this global to use all over the application

CONFIG.app          = process.env.APP   || 'dev';
CONFIG.port         = process.env.PORT  || '3008';

CONFIG.db_dialect   = process.env.DB_DIALECT    || 'mysql';
CONFIG.db_host      = process.env.DB_HOST       || '143.95.243.197';
CONFIG.db_port      = process.env.DB_PORT       || '3306';
CONFIG.db_name      = process.env.DB_NAME       || 'webdesig_zapkart';
CONFIG.db_user      = process.env.DB_USER       || 'webdesig_zapkart';
CONFIG.db_password  = process.env.DB_PASSWORD   || 'X(y9MNB;d4~D';

CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION || 'wedding-#app';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '365000000';
CONFIG.uploadpath = '/home/acer/Restaurent-app-node/teeta/teetaAdmin/upload/';

module.exports = CONFIG;
