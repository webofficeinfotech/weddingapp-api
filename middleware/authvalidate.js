const redis = require('redis');
const redisClient = redis.createClient();
const { to, ReE, ReS } = require('../services/util.service');
const CONFIG        = require('../config/config');


let authCache = async function (req, res, next) {
   
    
    const { username } = req.body.userName;

  redisClient.get(req.body.userName, function (error, cachedData) {
    if (error) {
        throw error;
    }
        if (cachedData === req.headers.authorization) {
            next();
            
    } else {
        return ReE(res, "Unauthenticated");
    }
});


}
module.exports.authCache = authCache;